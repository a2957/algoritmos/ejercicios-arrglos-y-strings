class Arreglo {
    constructor() {
        this.length = 0;
        this.data = {};
    }

    push(value) {
        this.data[this.length] = value;
        this.length++;
        return this.length;
    }

    get(index) {
        return this.data[index];
    }

    pop(index) {
        const lastItem = this.data[this.length - 1];
        delete this.data[this.length - 1];
        this.length--;
        return lastItem;
    }
    /*
    Ejercicio 2: Completa la clase con los siguientes métodos:
        1. deleteAtIndex → borrar un elemento del arreglo dado un indice
        2. shiftItems → Sobrescribir un elemento de un arreglo por el siguiente dado un indice
    */
    shiftItems(index){
    for(let i = 0; i < this.length; i++){
      if(i === index){
        delete this.data[i];
      }
    }

    let tempArray = this.data;
    this.data = {};
    let count = 0;
    for(let i = 0; i < this.length; i++){
      if(tempArray[i] !== undefined){
        this.data[count] = tempArray[i];
        count++;
      }
    }
    this.length--;
	}   
    
    deleteAtIndex(index){
    let itemDeleted = this.data[index];
    this.shiftItems(index);
    return itemDeleted;
	}
}

const personArray = new Arreglo();

personArray.push("Luis");
personArray.push("Diana");
personArray.push("Juan");
personArray.push("Julio");
console.log(personArray.data);

// personArray.pop()


console.log(personArray.deleteAtIndex(2));

console.log(personArray.data);
