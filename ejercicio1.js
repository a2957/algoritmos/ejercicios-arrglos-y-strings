function isUniqueChars(s){
  if(s.length > 128){
    return false;
  }

  let sArray = new Array(127).fill(false);

  for(let i = 0; i < s.length; i++){
    if(sArray[s.charCodeAt(i)] === true){
      return false;
    }else{
      sArray[s.charCodeAt(i)] = true;
    }
  }
  return true;
}

const result = isUniqueChars("abcdef");
